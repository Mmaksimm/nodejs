const { UserRepository } = require('../repositories/userRepository');

class UserService {

  // TODO: Implement methods to work with user
  get(id = false) {
    try {
      if (id) {
        const user = UserRepository.getOne(id);
        if (!user) {
          throw new Error('User not found!')
        }
      } else {
        return UserRepository.getAll()
      }
    } catch (err) {
      err.message = err.message
        ? err.message
        : 'User not found'
      throw err;
    }

  };

  post(data = false) {
    try {
      if (data) {
        try {
          this.get({ email: data.email });
        } catch (err) {
          console.log(data.email)
          return UserRepository.create(data);
        }
        throw new Error('This user already exists.');
      }
    } catch (err) {
      err.message = err.message
        ? err.message
        : 'User not created'
      throw err;
    }
  }

  put(id = false, dataToUpdate = false) {
    try {
      if (id && dataToUpdate) {
        if (this.get({ id: id })) {
          return UserRepository.update(id, dataToUpdate);
        };
      }
    } catch (err) {
      err.message = err.message
      throw err;
    }
  }

  delete(id = false) {
    try {
      if (id) {
        if (this.get(id)) {
          return UserRepository.delete(id)
        };
      };
    } catch (err) {
      throw err;
    }
  }



  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new UserService();