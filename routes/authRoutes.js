const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', async (req, res, next) => {
  // TODO: Implement login action (get the user if it exist with entered credentials)

  try {
    if (req.body.email) {
      console.log(req.body.email)
      const user = AuthService.login({ email: req.body.email });

      if (user.password !== req.body.password) {
        throw new Error('Wrong password.')
      }
      res.locals = {
        bodyAnswer: user
      }
    }

  } catch (err) {

    const message = err.message
      ? err.message
      : 'The user is not logged in.';
    const status = message === 'User not found'
      ? 404
      : 400;

    res.locals = {
      status: status,
      errorMessage: message
    }

  } finally {
    next();
  }
}, responseMiddleware);

module.exports = router;