const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
  // TODO: Implement methods to work with fighters
  get(id = false) {
    try {
      if (id) {
        const Fighter = FighterRepository.getOne({ id: id });
        if (!Fighter) {
          throw new Error('Fighter not found!')
        }
      } else {
        return FighterRepository.getAll()
      }
    } catch (err) {
      err.message = err.message
        ? err.message
        : 'Fighter not found'
      throw err;
    }

  }

  post(data = false) {
    try {
      if (data) {
        try {
          this.get({ email: data.email });
        } catch (err) {
          console.log(data.email)
          return FighterRepository.create(data);
        }
        throw new Error('This fighter already exists.');
      }
    } catch (err) {
      err.message = err.message
        ? err.message
        : 'Fighter not created'
      throw err;
    }
  }

  put(id = false, dataToUpdate = false) {
    try {
      if (id && dataToUpdate) {
        if (this.get({ id: id })) {
          return FighterRepository.update(id, dataToUpdate);
        };
      }
    } catch (err) {
      err.message = err.message
      throw err;
    }
  }

  delete(id = false) {
    try {
      if (id) {
        if (this.get(id)) {
          return FighterRepository.delete(id)
        };
      };
    } catch (err) {
      throw err;
    }
  }

}

module.exports = new FighterService();