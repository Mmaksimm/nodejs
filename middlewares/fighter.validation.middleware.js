const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during creation

  validation(req, res, 'Fighter not created');
  next();
}

const updateFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during update

  validation(req, res, 'Fighter not created');
  next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;

function validation(req, res, standartErrorMessage) {
  try {
    const keys = Object.keys(fighter);
    const body = {}

    keys.forEach(key => {
      if (key !== 'id' && req.body[key]) {
        body[key] = typeof req.body[key] === 'number'
          ? req.body[key]
          : req.body[key].trim()
      }
    });

    if (!body.health) {
      body.health = 100;
    }

    if ((keys.length - 1) !== Object.keys(body).length) {
      const message = keys.filter(key => {
        return !body[key] && (key !== 'id')
      }).join(', ');
      throw new Error(`${message} field is empty`)
    };

    if (!(/^\w{3,}$/.test(body.name))) {
      throw new Error('The name is incorrect')
    };

    if ((typeof body.power !== 'number') || body.power > 100 || body.power < 0) {
      throw new Error('Attack power must be a number between 0 and 100.')
    };

    if ((typeof body.defense !== 'number') || body.defense > 100 || body.defense < 0) {
      throw new Error('Defense power must be a number between 0 and 100.')
    };

    req.body = JSON.parse(JSON.stringify(body))

  } catch (err) {
    const message = err.message
      ? err.message
      : standartErrorMessage

    res.locals = {
      status: 400,
      errorMessage: message
    };
  } finally {
    return;
  };
};