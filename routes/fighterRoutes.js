const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter
router.get(['/', '/:id'], (req, res, next) => {
  try {
    if (!req.params.id) {
      res.locals = {
        bodyAnswer: FighterService.get()
      }
    } else {
      res.locals = {
        bodyAnswer: FighterService.get(req.params.id)
      }
    }
  } catch (err) {
    const message = err.message
      ? err.message
      : !req.params.id
        ? `Fighters not found!`
        : `Fighter not found!`

    res.locals = {
      status: 404,
      errorMessage: message
    };
  } finally {
    next();
  }
}, responseMiddleware);


router.post('/', createFighterValid, (req, res, next) => {
  try {
    if (!res.locals.status && Object.keys(req.body).length) {
      res.locals = {
        bodyAnswer: FighterService.post(req.body)
      }
    }
  } catch (err) {
    const message = err.message
      ? err.message
      : 'Fighter data not created'

    res.locals = {
      status: 400,
      errorMessage: message
    };
  } finally {
    next();
  }
}, responseMiddleware);

router.put(['/', '/:id'], updateFighterValid, (req, res, next) => {
  try {
    if (!res.locals.status && req.params.id && Object.keys(req.body).length) {
      res.locals = {
        bodyAnswer: FighterService.put(req.params.id, req.body)
      }
    }
  } catch (err) {
    const message = err.message
      ? err.message
      : 'Fighter data not updated'

    res.locals = {
      status: 404,
      errorMessage: message
    };
  } finally {
    next();
  }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
  try {
    res.json(FighterService.delete(req.params.id));

  } catch (err) {
    const message = err.message
      ? err.message
      : 'Fighter data not deleted'

    res.locals = {
      status: 400,
      errorMessage: message
    };
  } finally {
    next();
  }
}, responseMiddleware);

module.exports = router;