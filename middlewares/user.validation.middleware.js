const { user } = require('../models/user');

const createUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during creation

  validation(req, res, 'Account not created');
  next();
}

const updateUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during update

  validation(req, res, 'Account not updated');
  next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;

function validation(req, res, standartErrorMessage) {
  try {
    const keys = Object.keys(user);
    const body = {}

    keys.forEach(key => {
      if (key !== 'id' && req.body[key]) {
        body[key] = req.body[key].trim();
      }
    });

    if ((keys.length - 1) !== Object.keys(body).length) {
      const message = keys.filter(key => {
        return !body[key] && (key !== 'id')
      }).join(', ');
      throw new Error(`${message} field is empty`)
    };

    if (!(/^\w{3,}$/.test(body.firstName))) {
      throw new Error('The first name is incorrect')
    };

    if (!(/^\w{3,}$/.test(body.lastName))) {
      throw new Error('The last name is incorrect')
    };

    if (!(/^\w+@gmail.com$/.test(body.email))) {
      throw new Error('Please enter your email address in the format login@gmail.com')
    };

    if (!(/^\+380\d{9}$/.test(body.phoneNumber))) {
      throw new Error('Please enter the telephone number in the format + 380xxxxxxxxx.')
    };


    if (body.password.length < 7) {
      throw new Error('The password must be at least 7 characters long')
    };

    req.body = JSON.parse(JSON.stringify(body))

  } catch (err) {
    const message = err.message
      ? err.message
      : standartErrorMessage

    res.locals = {
      status: 400,
      errorMessage: message
    };
  } finally {
    return;
  };
};