const responseMiddleware = (req, res, next) => {
  // TODO: Implement middleware that returns result of the query
  if ((res.locals.status && res.locals.status === 400) || (res.locals.status && res.locals.status === 404)) {
    res.status(res.locals.status);
    res.json({
      error: true,
      message: res.locals.errorMessage
    });
  };

  res.json(res.locals.bodyAnswer)
  next();
}

exports.responseMiddleware = responseMiddleware;