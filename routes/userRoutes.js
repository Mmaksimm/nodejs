const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user
router.get(['/', '/:id'], (req, res, next) => {
  try {
    if (!req.params.id) {
      res.locals = {
        bodyAnswer: UserService.get()
      }
    } else {
      res.locals = {
        bodyAnswer: UserService.get(req.params.id)
      }
    }
  } catch (err) {
    const message = err.message
      ? err.message
      : !req.params.id
        ? `Users not found!`
        : `User not found!`

    res.locals = {
      status: 404,
      errorMessage: message
    };
  } finally {
    next();
  }
}, responseMiddleware);

router.post('/', createUserValid, (req, res, next) => {
  try {
    if (!res.locals.status && Object.keys(req.body).length) {
      res.locals = {
        bodyAnswer: UserService.post(req.body)
      }
    }
  } catch (err) {
    const message = err.message
      ? err.message
      : 'User data not created'

    res.locals = {
      status: 400,
      errorMessage: message
    };
  } finally {
    next();
  }
}, responseMiddleware);


router.put(['/', '/:id'], updateUserValid, (req, res, next) => {
  try {
    if (!res.locals.status && req.params.id && Object.keys(req.body).length) {
      res.locals = {
        bodyAnswer: UserService.put(req.params.id, req.body)
      }
    }
  } catch (err) {
    const message = err.message
      ? err.message
      : 'User data not updated'

    res.locals = {
      status: 404,
      errorMessage: message
    };
  } finally {
    next();
  }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
  try {
    res.json(UserService.delete(req.params.id));

  } catch (err) {
    const message = err.message
      ? err.message
      : 'User data not deleted'

    res.locals = {
      status: 400,
      errorMessage: message
    };
  } finally {
    next();
  }
}, responseMiddleware);

module.exports = router;